<?php
namespace FilesServiceClient;

use Carbon\Carbon;
use GuzzleHttp\Client;

/**
 * Class FilesServiceCLient
 * This is a client for Files Service
 *
 * @package App\Libs\Files
 */
class ServiceClient
{

    const RETURN_DOWNLOAD_URL  = 0;
    const RETURN_FULL_RESPONSE = 1;

    private $client      = null;
    private $jwt_token   = "";
    private $return_type = self::RETURN_DOWNLOAD_URL;

    public function __construct($jwt_token, $return_type = null)
    {
        // a token to sign a request
        $this->jwt_token = $jwt_token;

        // a type of returning data (direct download URL or full JSON structure)
        if(!$return_type) {
            $this->return_type = self::RETURN_DOWNLOAD_URL;
        }

        $this->initHttpClient();
    }

    /**
     * Make a HTTP client and set to this instance
     */
    private function initHttpClient()
    {
        $this->client = new Client([
                                       // Base URI is used with relative requests
                                       'base_uri' => 'https://files.babystep.tv',
                                       'headers'  => [
                                           'Authorization' => 'Bearer ' . $this->jwt_token,
                                       ],
                                   ]);
    }

    /**
     * Upload a local file to files service
     *
     * @param             $local_path
     * @param array       $payload
     * @param null|Carbon $expired_at
     */
    public function uploadFileFromLocal($local_path, $payload = [], $expired_at = null)
    {
        $multipart = [
            [
                'name'     => 'payload_json',
                'contents' => json_encode($payload),
            ],
            [
                'name'     => 'file',
                'contents' => fopen($local_path, 'r'),
            ],
        ];

        if(!is_null($expired_at)) {
            $multipart[] = [
                'name'     => 'remove_at',
                'contents' => $expired_at->toDateString() // is it working?
            ];
        }

        $response = $this->client->request('POST', '/', [
            'multipart' => $multipart,
        ]);

        $json_response = json_decode((string)$response->getBody(), true);

        // return just a link to download uploaded file
        if($this->return_type == self::RETURN_DOWNLOAD_URL) {
            return $json_response['data']['download_url'];
        }

        return $json_response;
    }

    /**
     * Upload a file to files service from remote URL
     *
     * @param       $local_path
     * @param array $payload
     * @param null  $expired_at
     */
    public function uploadFileFromRemoteUrl($remote_url, $payload = [], $expired_at = null)
    {

        $multipart = [
            [
                'name'     => 'downloadUrl',
                'contents' => $remote_url,
            ],
            [
                'name'     => 'payload_json',
                'contents' => json_encode($payload),
            ],
        ];

        if(!is_null($expired_at)) {
            $multipart[] = [
                'name'     => 'remove_at',
                'contents' => $expired_at->toDateString() // is it working?
            ];
        }

        $response = $this->client->request('POST', '/', [
            'multipart' => $multipart,
        ]);

        $json_response = json_decode((string)$response->getBody(), true);

        // return just a link to download uploaded file
        if($this->return_type == self::RETURN_DOWNLOAD_URL) {
            return $json_response['data']['download_url'];
        }

        return $json_response;

    }

    /**
     * This command will remove file from files repo
     *
     * @param INT|string $file this is a download link or ID of the file
     */
    public function removeFile($file) {

        $file_id = null;

        if(preg_match("#^\d+$#", $file)) {
            // this is ID
            $file_id = $file;
        } else {
            // this is string - URL
            // I need to detect a File ID from it
            if(preg_match("#/(\d+)-(.*)/download$#", $file, $p)) {
                $file_id = $p[1];
            }
        }

        if(!$file_id) {
            throw new \InvalidArgumentException("File argument is not a valid file ID nor a file download URL");
        }

        // todo make a file deletion

    }

}